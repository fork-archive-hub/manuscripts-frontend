/*!
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the “License”); you may not use this file except in compliance with the License. You may obtain a copy of the License at https://mpapp-public.gitlab.io/manuscripts-frontend/LICENSE. The License is based on the Mozilla Public License Version 1.1 but Sections 14 and 15 have been added to cover use of software over a computer network and provide for limited attribution for the Original Developer. In addition, Exhibit A has been modified to be consistent with Exhibit B.
 *
 * Software distributed under the License is distributed on an “AS IS” basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for the specific language governing rights and limitations under the License.
 *
 * The Original Code is manuscripts-frontend.
 *
 * The Original Developer is the Initial Developer. The Initial Developer of the Original Code is Atypon Systems LLC.
 *
 * All portions of the code written by Atypon Systems LLC are Copyright (c) 2019 Atypon Systems LLC. All Rights Reserved.
 */

import { UserProfile } from '@manuscripts/manuscripts-json-schema'
import React from 'react'

import { Loading } from '../components/Loading'
import { buildUser } from '../lib/data'
import { Collection } from '../sync/Collection'
import CollectionManager from '../sync/CollectionManager'
import { selectors } from '../sync/syncEvents'
import { SyncStateContext } from '../sync/SyncStore'
import { DataComponent } from './DataComponent'

interface Props {
  children: (
    data: UserProfile | null,
    collection: Collection<UserProfile>
  ) => React.ReactNode
  userProfileID: string
}

interface State {
  data?: UserProfile | null
}

class OptionalUserData extends DataComponent<UserProfile, Props, State> {
  public constructor(props: Props) {
    super(props)

    this.state = {}

    this.collection = CollectionManager.getCollection<UserProfile>('user')
  }

  public componentDidMount() {
    const { userProfileID } = this.props

    // TODO: handle "error"?
    this.sub = this.subscribe(userProfileID)
  }

  public componentWillReceiveProps(nextProps: Props) {
    const { userProfileID } = nextProps

    if (userProfileID !== this.props.userProfileID) {
      if (this.sub) {
        this.sub.unsubscribe()
      }

      this.setState({ data: undefined })
      this.sub = this.subscribe(userProfileID)
    }
  }

  public componentWillUnmount() {
    this.sub.unsubscribe()
  }

  public render() {
    const { data } = this.state

    if (data === undefined) {
      return this.props.placeholder || <Loading />
    }

    return (
      <SyncStateContext.Consumer>
        {({ syncState }) => {
          if (!selectors.isInitialPullComplete('user', syncState)) {
            return this.props.placeholder || <Loading />
          } else {
            return this.props.children(data, this.collection)
          }
        }}
      </SyncStateContext.Consumer>
    )
  }

  private subscribe = (userProfileID: string) =>
    this.collection.findOne(userProfileID).$.subscribe(async (doc) => {
      this.setState({
        data: doc ? await buildUser(doc) : null,
      })
    })
}

export default OptionalUserData
