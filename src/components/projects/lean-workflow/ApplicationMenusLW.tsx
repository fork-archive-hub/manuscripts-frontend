/*!
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the “License”); you may not use this file except in compliance with the License. You may obtain a copy of the License at https://mpapp-public.gitlab.io/manuscripts-frontend/LICENSE. The License is based on the Mozilla Public License Version 1.1 but Sections 14 and 15 have been added to cover use of software over a computer network and provide for limited attribution for the Original Developer. In addition, Exhibit A has been modified to be consistent with Exhibit B.
 *
 * Software distributed under the License is distributed on an “AS IS” basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for the specific language governing rights and limitations under the License.
 *
 * The Original Code is manuscripts-frontend.
 *
 * The Original Developer is the Initial Developer. The Initial Developer of the Original Code is Atypon Systems LLC.
 *
 * All portions of the code written by Atypon Systems LLC are Copyright (c) 2019 Atypon Systems LLC. All Rights Reserved.
 */

import {
  ApplicationMenus,
  DialogController,
  DialogNames,
  getMenus,
  MenuSpec,
  useApplicationMenus,
  useEditor,
} from '@manuscripts/manuscript-editor'
import { ContainedModel } from '@manuscripts/manuscript-transform'
import { Model, Project } from '@manuscripts/manuscripts-json-schema'
import { History } from 'history'
import React, { useState } from 'react'
import styled from 'styled-components'

import config from '../../../config'
import { remaster } from '../../../lib/bootstrap-manuscript'
import { addColor, buildColors } from '../../../lib/colors'
import {
  buildExportMenu,
  buildExportReferencesMenu,
} from '../../../lib/project-menu'
import { ExportFormat } from '../../../pressroom/exporter'
import { Collection } from '../../../sync/Collection'
import { SaveModel } from '../../inspector/StyleFields'
import { ModalProps } from '../../ModalProvider'
import { Exporter } from '../Exporter'
import { Importer, importManuscript } from '../Importer'

export const ApplicationMenuContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`

interface Props {
  editor: ReturnType<typeof useEditor>
  history: History
  project: Project
  manuscriptID: string
  addModal: ModalProps['addModal']
  collection: Collection<ContainedModel>
  modelMap: Map<string, Model>
  saveModel: SaveModel
  contentEditable: boolean
}

export const ApplicationMenusLW: React.FC<Props> = ({
  history,
  editor,
  addModal,
  manuscriptID,
  modelMap,
  saveModel,
  project,
  collection,
  contentEditable,
}) => {
  const openImporter = () => {
    addModal('importer', ({ handleClose }) => (
      <Importer
        handleComplete={handleClose}
        importManuscript={(models: Model[], redirect = true) =>
          importManuscript(models, project._id, collection, history, redirect)
        }
      />
    ))
  }
  const openExporter = (format: ExportFormat, closeOnSuccess: boolean) => {
    addModal('exporter', ({ handleClose }) => (
      <Exporter
        format={format}
        getAttachment={collection.getAttachmentAsBlob}
        handleComplete={handleClose}
        modelMap={modelMap}
        manuscriptID={manuscriptID}
        project={project}
        closeOnSuccess={closeOnSuccess}
      />
    ))
  }

  const projectMenu: MenuSpec = {
    id: 'project',
    label: 'Project',
    submenu: [
      buildExportMenu(openExporter),
      buildExportReferencesMenu(openExporter, editor.state),
      {
        role: 'separator',
      },
      {
        id: 'remaster',
        label: 'Remaster',
        run: () => remaster(editor.state, modelMap, project, manuscriptID),
      },
    ],
  }

  const developMenu: MenuSpec = {
    id: 'develop',
    label: 'Develop',
    submenu: [
      {
        id: 'import',
        label: 'Import Manuscript…',
        run: openImporter,
      },
    ],
  }

  const helpMenu: MenuSpec = {
    id: 'help',
    label: 'Help',
    submenu: [
      {
        id: 'documentation',
        label: 'Documentation',
        run: () => window.open('https://support.manuscripts.io/'),
      },
      {
        role: 'separator',
      },
      {
        id: 'project-diagnostics',
        label: 'View Diagnostics',
        run: () => history.push(`/projects/${project._id}/diagnostics`),
      },
    ],
  }

  const [dialog, setDialog] = useState<DialogNames | null>(null)
  const closeDialog = () => setDialog(null)
  const openDialog = (dialog: DialogNames) => setDialog(dialog)
  const { colors, colorScheme } = buildColors(modelMap)
  const handleAddColor = addColor(colors, saveModel, colorScheme)

  const menu = [
    projectMenu,
    ...getMenus(editor, openDialog, config.features.footnotes, contentEditable),
    helpMenu,
  ]

  if (!config.production) {
    menu.push(developMenu)
  }

  const menus = useApplicationMenus(menu)

  return (
    <>
      <DialogController
        currentDialog={dialog}
        handleCloseDialog={closeDialog}
        colors={colors}
        handleAddColor={handleAddColor}
        editorState={editor.state}
        dispatch={editor.dispatch}
      />
      <ApplicationMenus {...menus} />
    </>
  )
}
